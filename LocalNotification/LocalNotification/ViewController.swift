//
//  ViewController.swift
//  LocalNotification
//
//  Created by Danilo Angamarca on 10/1/18.
//  Copyright © 2018 Danilo Angamarca. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    @IBOutlet weak var InputTextfield: UITextField!
    
    @IBOutlet weak var labelText: UILabel!
    
    var notificationsMessage = "El texto"
    
    
    @IBAction func GuardarButton(_ sender: Any) {
        labelText.text = InputTextfield.text
        //Instanciar UserDefaults
        let defaults = UserDefaults.standard
        
        //Guardar Variables en dafaults
        // INt, bool, Double, String,
        
        defaults.set(InputTextfield, forKey: "label")
        
        //notificationsMessage += labelText.text!
    }
    
    func sendNotifications(){
        notificationsMessage += labelText.text!
         //1. Authorizations request (esta en didload)
        //2. Crear contenido de la notificacion
        
        let content = UNMutableNotificationContent()
        content.title = "Notificacion title"
        content.subtitle = "Notificacion subtitle"
        content.body = notificationsMessage
        
        let trigger =  UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        //4. definir un indetifier para la notificacion
        let identifier = "Notificacion"
        
        //5. Crear un Request
        let notificacionReques = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        //6. añadir el request al UNUsernotificationsCenter
        UNUserNotificationCenter.current().add(notificacionReques){ (error) in
            
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //1. Instanciar UsersDefaults
        let defaults = UserDefaults.standard
        
        //2. Accder al valor por medio del key
        labelText.text = defaults.object(forKey: "label") as? String
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]){ (granted, error) in }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    

}

